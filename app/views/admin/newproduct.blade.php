@extends('layouts/main')
@section('title')
    Add New Product
@stop
@section('content')
    <h3>Add New Product:</h3>


    <hr>
    {{Form::open(array('route'=>'storeProduct','files'=>'true'))}}
    <p>{{Form::label(' name','product name:')}}</p>
    <p>{{Form::text('name')}}</p>
    <p>{{$errors->first('name')}}</p>


    <p>{{Form::label('describtion','product Describtion:')}}</p>
    <p>{{Form::textarea('describtion')}}</p>
    <p>{{$errors->first('describtion')}}</p>


    <p>{{Form::label('price',' price:')}}</p>
    <p>{{Form::text('price')}}</p>
    <p>{{$errors->first('price')}}</p>


    <p>{{Form::label(' category_id','product category:')}}</p>
    <p>{{Form::select('category_id',$categories)}}</p>
    <p>{{$errors->first('category_id')}}</p>

    <p>{{Form::label('image','product image:')}}</p>
    <p>{{Form::file('image')}}</p>
    <p>{{$errors->first('image')}}</p>


    <p>{{Form::submit('Add')}}</p>
    {{Form::close()}}


@stop
